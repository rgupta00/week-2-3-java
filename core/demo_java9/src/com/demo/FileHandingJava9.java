package com.demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileHandingJava9 {

	public static void main(String[] args) throws IOException {
		Files.lines(Path.of("data.txt")).forEach(line-> System.out.println(line));
	}
}
