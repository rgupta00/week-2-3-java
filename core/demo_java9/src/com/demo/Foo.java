package com.demo;

public interface Foo {
	public void foof();

	public default void foof2() {
		foofPrivate();
	}

	public default void foof3() {
		foofPrivate();
	}

	public static void foofStatic() {

	}

	private void foofPrivate() {
		System.out.println("hello i am private method inside interface...");
	}

}
