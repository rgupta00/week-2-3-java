package com.productapp;

public class Product implements Comparable<Product> {
	private int productId;
	private String productName;
	private double productPrice;
	private int productDiscount;
	

	public Product(int productId, String productName, double productPrice, int productDiscount) {
		this.productId = productId;
		this.productName = productName;
		this.productPrice = productPrice;
		this.productDiscount = productDiscount;
	}
	

	public Product() {}

	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	public int getProductDiscount() {
		return productDiscount;
	}
	public void setProductDiscount(int productDiscount) {
		this.productDiscount = productDiscount;
	}
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice
				+ ", productDiscount=" + productDiscount + "]";
	}


	@Override
	public int compareTo(Product o) {
		return Integer.compare(this.getProductId(), o.getProductId());
	}
	
	
	
}
