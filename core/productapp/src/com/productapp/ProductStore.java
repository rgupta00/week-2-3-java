package com.productapp;

public interface ProductStore {

	void printProductDetails();

	void printSortedProductsAsPerId();

	void printSortedProductsAsPerPrice();

}