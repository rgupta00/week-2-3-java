package com.day12.session1.formhouse;

import java.util.*;
import java.util.function.Predicate;

public class AppleAppMain {

	public static void main(String[] args) {
		List<Apple> apples = Arrays.asList(
				new Apple("red", 590),
				new Apple("green", 290),
				new Apple("green", 200),
				new Apple("red", 300), 
				new Apple("red", 250),
				new Apple("red", 400));

		Predicate<Apple> heavyApplesPredicate = apple -> apple.getWeight() >= 300;

		List<Apple> heavyApples = AppleApp.getAllApplesOnPredicate(apples, heavyApplesPredicate);
		System.out.println("----printing the heavy apples----------");
		for (Apple temp : heavyApples) {
			System.out.println(temp);
		}

		Predicate<Apple> greenApplePreicate = a-> a.getColor().equals("green");

		List<Apple> greenApples = AppleApp.getAllApplesOnPredicate(apples, greenApplePreicate);
		System.out.println("----printing the selected green apples----------");
		for (Apple temp : greenApples) {
			System.out.println(temp);
		}

		
		//all green or heavy apples
		Predicate<Apple>predicateHeavyOrGreen=heavyApplesPredicate.or(greenApplePreicate);
		List<Apple> greenOrHeavyApples = AppleApp.getAllApplesOnPredicate(apples, predicateHeavyOrGreen);
		System.out.println("----printing the  green  or heavy apples----------");
		for (Apple temp : greenOrHeavyApples) {
			System.out.println(temp);
		}
		
		
		//P2 : heavy and green
		Predicate<Apple> p2 = heavyApplesPredicate.and(greenApplePreicate);
		
//		
//		List<Apple> greenApples=AppleApp.getAllGreenApples(apples);
//		System.out.println("----printing the selected apples----------");
//		for(Apple temp: greenApples) {
//			System.out.println(temp);
//		}
//		
//		List<Apple> heavyApples=AppleApp.getAllHeavyApples(apples);
//		System.out.println("----printing the selected heavy apples----------");
//		for(Apple temp: heavyApples) {
//			System.out.println(temp);
//		}
	}
}
