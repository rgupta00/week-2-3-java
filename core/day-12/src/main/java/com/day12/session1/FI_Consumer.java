package com.day12.session1;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class FI_Consumer {
	
	public static void main(String[] args) {
		/*
		 * 1. predicate
		 * 2. funcation
		 * 3. consumer
		 * 4. Supplier
		 */
		
		Employee emp=new Employee(123, "raj", 60, 43);
		Consumer<Employee> empConsumer=e-> System.out.println(e);
		empConsumer.accept(emp);
		
		
		
		
		
		
		Consumer<String> consumer= name-> System.out.println(name);
		
		consumer.accept("java 8 training");
		
		
		
		//Consumer : dont return anything it simply take a argumnent: printing the data 
		
		//BiConsumer : .......................take 2 values 
		
		BiConsumer<Employee, String> biConsumer=(e, add)-> System.out.println(e + " : "+ add);
		
	}

}
