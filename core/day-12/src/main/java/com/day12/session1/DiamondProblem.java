package com.day12.session1;

interface A{
	default void foo() {
		System.out.println("default method of A class");
	}
}
interface B{
	default void foo() {
		System.out.println("default method of B class");
	}
}
class C implements A, B{
	public void foo() {
		A.super.foo();
		B.super.foo();
		System.out.println("foo is overrident");
	}
}
public class DiamondProblem {

	public static void main(String[] args) {
		A a=new C();
		a.foo();
	}
}
