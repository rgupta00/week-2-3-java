package com.day12.session1;
//inerface evoluation (juddad)
//diamond problem : 

interface Foo{
	void foo();
	static void fooStatic() {
		System.out.println("Staitc method inside Foo class");
	}
	default void bar() {
		System.out.println("bar default imp....");
	}
}
class FooImpl implements Foo{
	@Override
	public void foo() {
		System.out.println("foo is implemented by FooImp1..");
	}
	public void bar() {
		System.out.println("bar default imp is overriden by FooImp...");
	}
	
}



public class InterfaceEvoluation {

	public static void main(String[] args) {
		Foo f=new FooImpl();
		Foo.fooStatic();
	}
}





