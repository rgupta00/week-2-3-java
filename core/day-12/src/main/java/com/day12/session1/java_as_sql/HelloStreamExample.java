package com.day12.session1.java_as_sql;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collector;
import java.util.stream.Collectors;
public class HelloStreamExample {

	public static void main(String[] args) {
		//hello world stream processing
		
		List<Employee> employees=getAllEmployees();
		//i want the name of employees getting salary more then or 
		//equals to  50 in sorted order of the salary
		
//		
//		List<String> namesList=employees.stream()
//				.sorted(Comparator.comparing(Employee::getSalary))
//				.filter(emp-> emp.getSalary()>=50)
//				.map(emp-> emp.getName())
//				.collect(Collectors.toList());
//		namesList.forEach(name-> System.out.println(name));
//				
		employees.stream()
				.sorted(Comparator.comparing(Employee::getSalary))
				.filter(emp-> emp.getSalary()>=50)
				.map(emp-> emp.getName())
				.forEach(name-> System.out.println(name));
				
		
		
		
		//what if u want to sort emp as per salary
		
//		List<Employee>employeesSorted=employees.stream().sorted().collect(Collectors.toList());
//		//internal iteation
//		Consumer<Employee> empConsumer=emp-> System.out.println(emp);
//		
//		employeesSorted.forEach(empConsumer);//when u pass a method into a other method call
//		// ie called higher order method : funcatinal progamming
		
		
//		List<Employee>employeesSortedAsPerSalary=employees.
//				stream()
//				.sorted(( o1,  o2)->  Integer.compare(o2.getSalary(), o1.getSalary()))
//				.collect(Collectors.toList());
//		
//		employeesSortedAsPerSalary.forEach(e-> System.out.println(e));
		
		
//		List<Employee>employeesSortedAsPerSalary=employees.
//				stream()
//				.sorted(Comparator.comparing(Employee::getSalary).reversed())
//				.collect(Collectors.toList());
//		
//		employeesSortedAsPerSalary.forEach(System.out::println);//method ref?
//		
	
		
//		//extrenal iteration: u tell java how to print it
//		for(Employee emp: employeesSorted) {
//			System.out.println(emp);
//		}
		
		/*
		 * select *
		 * from Employee
		 * where salary > 60;
		 */
		//i want the name of employees getting salary more then or equals to  50 in sorted order of the salary
		///java 7
		
//		
//		List<Employee> employees=getAllEmployees();
//		
//		//1. sort the emp as per salary
//		Collections.sort(employees, new Comparator<Employee>() {
//
//			@Override
//			public int compare(Employee o1, Employee o2) {
//			
//				return Integer.compare(o2.getSalary(), o1.getSalary());
//			}
//		});
//		
//		//2. somehow get the names getting salary more then or eq to 50
//		List<String> empNames=new ArrayList<String>();
//		for(Employee emp: employees) {
//			if(emp.getSalary()>=50)
//			empNames.add(emp.getName());
//			
//		}
//		
//		//3. printing them
//		for(String name: empNames) {
//			System.out.println(name);
//		}
	}
	
	public static List<Employee> getAllEmployees(){
		return Arrays.asList(
				new Employee(121, "pooja", 40, 43),
				new Employee(11, "amit", 41, 40),
				new Employee(91, "syed", 50, 23),
				new Employee(21, "sushant",50, 23),
				new Employee(41, "umesh", 55, 43),
				new Employee(81, "indu", 75, 45));
		
	}
}
