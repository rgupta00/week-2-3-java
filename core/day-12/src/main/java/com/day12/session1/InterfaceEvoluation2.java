package com.day12.session1;
/*
 * java 8: declaritive data processing : SQL
 * 
 * 	 declaritive data processing ==> Streams===> Funcatino Interface => lambada Exp ==> inteface evoluation
 * 
 */
interface Foof{
	void foof();
	default void foof2() {
		System.out.println("foof2 is a default method");
	}
	public static void foofStatic() {
		System.out.println("foof static methdod...");
	}
	
	
}
class FoofImp implements Foof{

	@Override
	public void foof() {
		System.out.println("foof method is overriden imp2");
	}
	
}

class FoofImp2 implements Foof{

	@Override
	public void foof() {
		System.out.println("foof method is overriden imp2");
	}
	
	public void foof2() {
		System.out.println("foof2 is overren by FooImp2");
	}
}
public class InterfaceEvoluation2 {

	public static void main(String[] args) {
		Foof foof=new FoofImp2();
		Foo.fooStatic();
	}
}





