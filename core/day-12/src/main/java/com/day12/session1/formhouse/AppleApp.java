package com.day12.session1.formhouse;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
public class AppleApp {
	//Behaviour design pattern: strategy design pattern *
	//Behavioural parametreization is a part of stratgy design pattern
	//SOLID principles: 
	
	public static List<Apple> getAllApplesOnPredicate(List<Apple>apples, Predicate<Apple>predicate){
		return apples.stream().filter(predicate).collect(Collectors.toList());
	}
	
	
	
//	public static List<Apple> getAllApplesOnPredicate(List<Apple>apples, Predicate<Apple>predicate){
//		List<Apple> tempList=new ArrayList<Apple>();
//		for(Apple apple: apples) {
//			if(predicate.test(apple))
//				tempList.add(apple);
//		}
//		return tempList;
//	}
	
	
//	public static List<Apple> getAllGreenApples(List<Apple>apples){
//		List<Apple> tempList=new ArrayList<Apple>();
//		for(Apple apple: apples) {
//			if(apple.getColor().equals("green"))
//				tempList.add(apple);
//		}
//		return tempList;
//	}
//
//	public static List<Apple> getAllHeavyApples(List<Apple>apples){
//		List<Apple> tempList=new ArrayList<Apple>();
//		for(Apple apple: apples) {
//			if(apple.getWeight()>= 300)
//				tempList.add(apple);
//		}
//		return tempList;
//	}
//	
//	public static List<Apple> getAllHeavyAndGreenApples(List<Apple>apples){
//		List<Apple> tempList=new ArrayList<Apple>();
//		for(Apple apple: apples) {
//			if(apple.getWeight()>= 300)
//				tempList.add(apple);
//		}
//		return tempList;
//	}
}




