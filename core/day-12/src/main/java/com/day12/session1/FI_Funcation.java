package com.day12.session1;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class FI_Funcation {
	
	public static void main(String[] args) {
		/*
		 * 1. predicate
		 * 2. funcation
		 * 3. consumer
		 * 4. Supplier
		 */
		
		//predicate :T ==> T/F
		Predicate<Integer> evenOddPredicate = n-> n%2==0?true:false;
		System.out.println(evenOddPredicate.test(49));	
		
		//Pricate check name contains raj
		
		Predicate<String>predicate= s->  s.contains("raj");
		System.out.println(predicate.test("rijesh"));
			
		BiPredicate<Integer, Integer> biPredicate=( n1,  n2)->  n1>n2?true:false;
		
		System.out.println(biPredicate.test(34, 120));
	
		//Preidate of user defined objects
		//is the emp is high salary getter : 80
		Predicate<Employee>predicate2= emp->  emp.getSalary()> 800000;
	
		System.out.println(predicate2.test(new Employee(12, "raj",9000, 43)));
		
	}

}
