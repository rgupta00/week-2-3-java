package com.day11.session2;

import java.util.function.Function;

import com.day11.session1.Employee;

public class DemoFuncation {
	
	public static void main(String[] args) {
		
		Function<Employee, String>function= emp-> emp.getName();
		
		Employee employee=new Employee(12, "neeraj", 56, 23);
		System.out.println(function.apply(employee));
			
		
	}

}
