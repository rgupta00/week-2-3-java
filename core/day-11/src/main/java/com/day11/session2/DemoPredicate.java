package com.day11.session2;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

import com.day11.session1.Employee;

public class DemoPredicate {
	
	public static void main(String[] args) {
		
		Predicate<Integer> evenOrOdd=t ->t%2==0?true:false;
		
		System.out.println(evenOrOdd.test(54));
		
		//i want to create a predicate for a String is that containe Mr. or not
		
		Predicate<String> prediateMr= data-> data.contains("MR.");
		
		System.out.println(prediateMr.test("MR. Coder"));
		
		//Predicat for emp > more then 50
		
		Predicate<Employee> predicateEmp=emp-> emp.getSalary()> 50;
		Predicate<Employee> predicateSrEmp=e-> e.getAge()> 60;
		
	//	BiPredicate<Integer, Integer> biPredicateGreater=(a,b)-> a>b?true:false;
		
		
		//System.out.println(biPredicateGreater.test(4, 41));
		
		BiPredicate<Employee, Employee> biPredicateEmp=(e1, e2)->e1.getSalary()>e2.getSalary()?true:false;
		
		
		
	}

}
