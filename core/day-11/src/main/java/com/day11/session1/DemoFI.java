package com.day11.session1;

@FunctionalInterface
interface Foof {
	// String toString();//FI should not have method of Object class
	public void foo();

	default void foof2() {
	}
}

public class DemoFI {

	public static void main(String[] args) {
		//lambda exp is a syntex suger on ann inner class XXXXX
		//not a syn suger it is performace impovement
		
//		Foof foo = new Foof() {
//
//			@Override
//			public void foo() {
//				System.out.println("foof is implemented");
//			}
//		};
			///lambda exp is implemented interanally as static method 
		
		Foof foo2 = ()-> System.out.println("foof is implemented");

		

			Foof foo3 = ()-> {
				System.out.println("foof is implemented");
			};
			
			Foof foo4 = ()-> {
				System.out.println("foof is implemented");
			};
			
		
	}
}





