package com.day11.session1;
import java.util.*;
public class DemoLambda {
	
	public static void main(String[] args) {
		
		List<Employee> employees=new ArrayList<Employee>();
		employees.add(new Employee(121, "raj", 50, 42));
		employees.add(new Employee(12, "ekta", 60, 40));
		employees.add(new Employee(109, "gunika", 70, 15));

		//Collections.sor
		
//		Comparator<Employee> comparator= new Comparator<Employee>() {
//
//			@Override
//			public int compare(Employee o1, Employee o2) {
//				return Integer.compare(o2.getSalary(), o1.getSalary());
//			}
//		};
		
		//Step 1
		
//		Comparator<Employee> comparator= (Employee o1, Employee o2) ->{
//				return Integer.compare(o2.getSalary(), o1.getSalary());
//			
//		};
//		
		//Step 2
//		Comparator<Employee> comparator= (Employee o1, Employee o2) ->
//			 Integer.compare(o2.getSalary(), o1.getSalary());
		
	    //step 3
		
//		Comparator<Employee> comparator= ( o1,  o2) ->
//		 Integer.compare(o2.getSalary(), o1.getSalary());
		 
		 //Step 4
		 										//method ref* is a syntex suger on lambda exp!
		Comparator<Employee> comparator= Comparator.comparing(Employee::getSalary).reversed();
		
	
		Collections.sort(employees, comparator);
		
		
		
		
		
		for(Employee employee: employees) {
			System.out.println(employee);
		}
		
		
		
		
		
//		Thread thread=new Thread(()-> System.out.println("job of the thread"));
//		thread.start();
	}

}
