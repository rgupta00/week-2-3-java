package com.day7.session2.collection;
import java.util.*;
import java.util.Map.Entry;
public class DemoMap {
	
	public static void main(String[] args) {
		
		System.out.println("raj".hashCode());
		Map<String, Integer> marks=new HashMap<String, Integer>(16, .75f);
		marks.put("raj", 80);
		
		marks.put("gunika", 90);
		marks.put("raj", 85);
		marks.put("neeraj", 91);
		marks.put("abdul", 89);
		
		//System.out.println(marks);
		
		//performance of insetion and getting data from the hashmap is 0(1)
		//System.out.println(marks.get("raj"));
		
		//System.out.println(marks.containsKey("jlkj"));
		
		//you need to get set of keys
//		Set<String> keySet = marks.keySet();
//		
//		for(String key : keySet) {
//			System.out.println(key + " : "+ marks.get(key));
//		}
		
		Set<Entry<String, Integer>> entrySet = marks.entrySet();
		for(Entry<String, Integer> entry: entrySet) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}
		
	}

}
