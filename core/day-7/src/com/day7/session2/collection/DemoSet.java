package com.day7.session2.collection;
import java.util.*;
class ReverseSort implements Comparator<String>{

	@Override
	public int compare(String o1, String o2) {
		return o2.compareTo(o1);
	}
	
}
public class DemoSet {
	
	public static void main(String[] args) {
		
		//U can also covert and array to arraylist
		List<String> data=Arrays.asList("foo","bar","jar","car","jar");
		Set<String> dataUnique=new TreeSet<String>(data);
		System.out.println(dataUnique);
		
		//set dont allow duplicate, hashset using hashing* and dont maintain the order of insertion
		
		Set<String> set=new TreeSet<String>(new ReverseSort());
		
		set.add("raj");
		set.add("java");
		set.add("training");
		set.add("raj");
		set.add("foo");
		//System.out.println(set);
	}

}
