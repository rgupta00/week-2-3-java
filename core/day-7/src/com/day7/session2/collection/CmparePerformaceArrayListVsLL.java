package com.day7.session2.collection;

import java.util.*;

public class CmparePerformaceArrayListVsLL {

	public static void main(String[] args) {
		List<Integer> list1 = new ArrayList<Integer>();
		List<Integer> list2 = new LinkedList<Integer>();

		doTiming(list1);

	}

	private static void doTiming(List<Integer> list) {
		long start = System.currentTimeMillis();
		//we are adding 1 lac elemnt in the list
		for(int i=0;i<1E5; i++) {
			list.add(i);
		}
		//adding 1 lac more element at the begi
		for(int i=0;i<1E5; i++) {
			list.add(0, i);
		}
		
		long end = System.currentTimeMillis();

		System.out.println("time taken : " + (end - start) + " ms");
	}
}





