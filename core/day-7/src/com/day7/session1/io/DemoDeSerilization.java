package com.day7.session1.io;
import java.io.*;
public class DemoDeSerilization {

	
	public static void main(String[] args) {
		Account account=null;
		//ser
		try (ObjectInputStream oos=new ObjectInputStream(new FileInputStream(new File("demo.ser")))){	
			account = (Account) oos.readObject();
			System.out.println(account);
		} catch (FileNotFoundException e) {
			System.out.println("file is not found");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("some io ex");
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
}
