package com.day7.session1.io;
import java.io.*;
public class DemoSerilization {

	
	public static void main(String[] args) {
		Account account=new Account(12, "raja", 400);
		account.setPassword("foo534545");
		//ser
		try (ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(new File("demo.ser")))){	
			oos.writeObject(account);
			System.out.println("done");
		} catch (FileNotFoundException e) {
			System.out.println("file is not found");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("some io ex");
		}
		
	}
}
