package com.day7.session1.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadData {
	// to read the file and create an collection of employoee
	//ArrayList : Java collection
	public static void main(String[] args) {
		try {
			BufferedReader br=new BufferedReader(new FileReader(new File("data.csv")));
			String line=null;
			while((line=br.readLine())!=null) {
				String tokens[]=line.split(",");
				for(String token: tokens) {
					System.out.print(token+" ");
				}
				System.out.println();
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
		
	}
}
