package com.day7.session1;
//exception wrapping?
//and rethroing

class MathException extends RuntimeException{
	public MathException(String message) {
		super(message);
	}
}
class MathLib{
	
	public static void divide(int x, int y)throws MathException {
		try {
			int z=x/y;
			System.out.println(z);
		}catch(ArithmeticException e) {
			//throw e;// excpetion rethrowing
			throw new MathException("divide by zero happens");//ex wrapping and rethrowing?
		}
	}
}

public class ExWrappingAndRethrowing {
	
	public static void main(String[] args) {
		try{
			MathLib.divide(5, 0);
		}catch(MathException e) {
			System.out.println(e.getMessage());
		}
	}

}
