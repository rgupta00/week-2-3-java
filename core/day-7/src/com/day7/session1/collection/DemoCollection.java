package com.day7.session1.collection;
import java.util.*; // package that contain collection  api

import com.day7.session1.io.Account;

public class DemoCollection {

	public static void main(String[] args) {
		//how to use collection correctly
		
		//generics provide type safty to the data
		ArrayList<String> list=new ArrayList<String>();
		list.add("foo");
		list.add("bar");
		list.add("java");
		list.add("python");
		list.add("c++");
		
		System.out.println("-----before sorting -------");
		System.out.println(list);
		System.out.println("-----after sorting -------");
		Collections.sort(list);
		System.out.println(list);
		//[bar, c++, foo, java, python]
		System.out.println("-------binary seach---------");
		
		int index= Collections.binarySearch(list, "pxthon");
		System.out.println(index);
		
		//data stru = data + 		structure +		 alog
				//what to store + where to store +  sort/search...?
		
		
		
		//how to print it==> syntex suger
//		for(String data: list) {
//			System.out.println(data);
//		}
		//Iterator : 
//		Iterator<String> it=list.iterator();
//		while(it.hasNext()) {
//			String val=it.next();
//			if(val.equals("bar")) {
//				it.remove();
//			}
//		}
//		
//		System.out.println(list);
		
		//ListIterator : 
		
		//CRUD:
		
//		ListIterator<String> it2=list.listIterator(list.size());
//		while(it2.hasPrevious()){
//			System.out.println(it2.previous());
//		}
//		
		//how to use AL
		
//		ArrayList list=new ArrayList();// Generics
//		//it hectrogenious
//		list.add("foo");
//		list.add(44);//java automatically box it
//		list.add(new Account(2, "raj", 200));
//		
//		//u will get problem while getting the itms, u dont know which index contain what
//		//u will lots of typcasting or instance : bad code
//		
//		for(Object o: list) {
//			
//			System.out.println(((Account) o).getName());
//		}
		
		
		
		
		
		
		//ArrayList	vs 		LinkedList
	//	int a[]=new int[10];
		//radom access is fast :)	0(1)
		//bad: fixed size, insertion/del is slow...
		//linklist: access is little slow O(n)
	//	ArrayList list=new ArrayList(10);
		//radom access is fast in AL :)
		//add/del is slow
		
	//	LinkedList list2=new LinkedList();
		//DLL
		//adv :add/del is fast
		//dis: accessig is little slow
		
	}
}
