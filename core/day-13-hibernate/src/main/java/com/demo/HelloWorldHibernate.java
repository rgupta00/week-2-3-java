package com.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HelloWorldHibernate {

	public static void main(String[] args) {
		
		StandardServiceRegistry serviceRegistry=new StandardServiceRegistryBuilder()
				.configure("hibernate.cfg.xml")
				.build();
		//first we create sessionfactory
		SessionFactory factory=new MetadataSources(serviceRegistry)
				.buildMetadata().buildSessionFactory();
		
		
		//from sessionfactory we need to obtain session
		
		Session session=factory.openSession(); //Session session2=factory.getCurrentSession();
		
		session.getTransaction().begin();
		
		Employee employee=new Employee(102, "umesh", 57, 45);
		employee.setPhone("444545454545");
		session.save(employee);
		
		
		session.getTransaction().commit();
		
		session.close();
		factory.close();
		
	}
}







