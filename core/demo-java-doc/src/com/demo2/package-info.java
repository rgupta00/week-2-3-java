/**
 * Provides the classes necessary to create an hello world calculator and the classes for cgi training
 * <p>
 * The applet framework involves two entities: 
 * the applet and the applet context. An applet is an embeddable window (see the 
 * {@link java.awt.Panel} class) with a few extra methods that the applet context 
 * can use to initialize, start, and stop the applet.
 *
 * @since 1.0
 * @see com.demo2
 */

package com.demo2;