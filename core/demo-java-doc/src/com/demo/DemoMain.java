package com.demo;
/**
* <h1>Hello, World!</h1>
* The HelloWorld program implements an application that
* simply displays "Hello World!" to the standard output.
* <p>
* Giving proper comments in your program makes it more
* user friendly and it is assumed as a high quality code.

* @author  Rajiv Gupta
* @version 1.0
* @since   2021-03-31 
*/
public class DemoMain {

	public static void main(String[] args) {
		Calculator calculator=new Calculator();
		System.out.println(calculator.addNum(2, 2));
	}
}
