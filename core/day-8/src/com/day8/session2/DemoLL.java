package com.day8.session2;

class LinkList{
	class Node{
		int data;
		Node next;
		public Node(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	Node head, tail=null;
	//add a node in front
	
	//delete a node from front
	
	//delete a node inbw
	
	//add a no inbw
	
	public void addNode(int data) {
		Node newNode=new Node(data);
		if(head==null) {
			head=tail=newNode;
		}else {
			tail.next=newNode;
			tail=newNode;
		}
	}
	
	public void display() {
		System.out.println("--------");
		Node current=head;
		if(current==null)
			System.out.println("nothing in LL");
		else {
			while(current!=null) {
				System.out.print(current.data+"->");
				current=current.next;
			}
		}
		System.out.println("--------");
	}
}

public class DemoLL {

	public static void main(String[] args) {
		LinkList linkList=new LinkList();
		linkList.addNode(44);
		linkList.addNode(4);
		linkList.addNode(94);
		linkList.addNode(3);
		
		linkList.display();
		
		
	}
}
