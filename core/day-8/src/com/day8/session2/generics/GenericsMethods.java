package com.day8.session2.generics;
class Student implements Comparable<Student>{
	private String name;
	private int marks;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	public Student(String name, int marks) {
		this.name = name;
		this.marks = marks;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", marks=" + marks + "]";
	}
	@Override
	public int compareTo(Student o) {
		return Integer.compare(this.getMarks(), o.getMarks());
	}
	
	
}
public class GenericsMethods {

	public static void main(String[] args) {
		Integer a, b, c;
		a = 44;
		b = 2;
		c = -77;
		Integer result = findMax(a, b, c);

		
		System.out.println(result);
		
		Student student1=new Student("raj", 47);
		Student student2=new Student("ekta", 48);
		
		Student student3=new Student("gunika", 49);
		
		Student maxResult=findMax(student1, student2, student3);
		System.out.println(maxResult);

	}

	static <T extends Comparable<T>>  T findMax(T a, T b, T c) {
		T max = a;
		if (b.compareTo(max) > 0) {
			max = b;
		}
		if (c.compareTo(max) > 0) {
			max = c;
		}
		return max;
	}
}
