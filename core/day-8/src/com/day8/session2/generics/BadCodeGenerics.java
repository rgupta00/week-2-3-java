package com.day8.session2.generics;
import java.util.*;
public class BadCodeGenerics {

	public static void main(String[] args) {
		//Java 1.5: what is generics: is a type errased?
		// generics is a compile time safty net? 
		//generics dont availale at run time .. JVM dnot know anything about generics
		
		//dont mix milk +wine
		//dont mix generics code and non generics code=> bugs
		
		List<Integer> list=new ArrayList<Integer>();
		list.add(44);
		
		strangeMethod(list);
		
		for(Integer temp: list) {
			System.out.println(temp);
		}
	}

	private static void strangeMethod(List list) {
		list.add("raj");
	}
}



