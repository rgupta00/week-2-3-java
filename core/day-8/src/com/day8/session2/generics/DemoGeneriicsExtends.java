package com.day8.session2.generics;

import java.awt.font.NumericShaper;
import java.util.*;

public class DemoGeneriicsExtends {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(464);
		list.add(440);
		list.add(414);
		System.out.println("-----arraylist of int-------");
		print(list);

		List<Double> list2 = new ArrayList<Double>();
		list2.add(464.70);
		list2.add(440.00);
		list2.add(414.90);
		System.out.println("-----arraylist of int-------");
		print(list2);

	}
	//what it means hey java i can pass any list whose member have something
	//to do with Numer (ie they are related to no)
	// u can print that list but u can not modify it
	
	//What is PECS (Producer Extends Consumer Super)?

	
	//? extends Object=== ?
	static void print(List<? extends Object> list) {
		
		
		Iterator<? extends Object> it=list.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
	}

}
