package com.day8.session2.generics;
class MyClass<T1,T2>{
	T1 i;
	T2 j;
	public T1 getI() {
		return i;
	}
	public void setI(T1 i) {
		this.i = i;
	}
	public T2 getJ() {
		return j;
	}
	public void setJ(T2 j) {
		this.j = j;
	}
}
public class DemoGenericClass {
	
	public static void main(String[] args) {
		MyClass<Integer, Integer> myclass=new MyClass<>();
		myclass.setI(44);
	}

}
