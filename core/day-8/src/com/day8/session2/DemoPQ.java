package com.day8.session2;

import java.util.*;
//FIFO
//class MyQeue{
//	private LinkedList<Integer> linkList=new LinkedList<Integer>();
//	public void insert(int data) {
//		linkList.addLast(data);
//	}
//	public int remove(int data) {
//		return linkList.removeFirst();
//	}
//}
class ReverseSorter implements Comparator<Integer>{

	@Override
	public int compare(Integer o1, Integer o2) {
		return Integer.compare(o2, o1);
	}
	
}
public class DemoPQ {

	public static void main(String[] args) {
		PriorityQueue<Integer> queue=new PriorityQueue<Integer>(new ReverseSorter());
		queue.offer(33);
		queue.offer(330);
		queue.offer(67);
		queue.offer(890);
		queue.offer(600);
//		System.out.println(queue.poll());
//		System.out.println(queue.poll());
//		System.out.println(queue.poll());
//		System.out.println(queue.poll());
//		System.out.println(queue.poll());
//		
	
//		Iterator<Integer> it=queue.iterator();
//		while(it.hasNext()){
//			System.out.println(it.next());
//		}
		
		while(true) {
			Integer val=queue.poll();
			if(val==null)
				break;
			else
				System.out.println(val);;
		}
		
		//job worker (
		//java 1.5
		//can u create queue using LL
		//............. stack using LL
		
		/*		throws ex		dont throw			block(BlockingQueue solve P&C problem)
		 * ______________________________________________
		 * insert:	add(e)	vs		offer(e)			put(e)
		 * remove	remove() vs		poll()				take()
		 * examine	element() vs	peek()				XXXX
		 */
		
		
	}
}
