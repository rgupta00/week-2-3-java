package com.day8.session2;

import java.util.*;
class Job implements Comparable<Job>{
	private int rate;
	private String jobName;
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	@Override
	public String toString() {
		return "Job [rate=" + rate + ", jobName=" + jobName + "]";
	}
	public Job(int rate, String jobName) {
		this.rate = rate;
		this.jobName = jobName;
	}
	public Job() {}
	@Override
	public int compareTo(Job o) {
		return Integer.compare(o.getRate(), this.getRate());
	}
	
	
}

public class DemoPQUserDefineObject {

	public static void main(String[] args) {
		PriorityQueue<Job> queue=new PriorityQueue<Job>();
		queue.offer(new Job(500, "make a dye for a sergical item"));
		queue.offer(new Job(5500, "make a dye for bone implants"));
		queue.offer(new Job(400, "make a dye for a toy"));
		queue.offer(new Job(5000, "make a dye for a airplan part"));
		
		System.out.println(queue.poll());
		
		
		
		
	}
}
