package com.day8.session1.finalize;

import java.util.Date;

import com.day8.session1.clonning.Customer;

public class DemoFinalize {

	public static void main(String[] args) {
		Customer customer=new Customer(1, "raj", 45, new Date());
		customer=null;
		System.gc();//it is polite req to gc
		Runtime.getRuntime().gc();////it is polite req to gc

		
		System.out.println("cust becore null");
	}
}
