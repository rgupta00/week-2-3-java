package com.day8.session1;
import java.util.*;
import java.util.Map.Entry;
public class HashMapUserDefnedObject {

	public static void main(String[] args) {
		
		Map<Employee, Integer> map=new TreeMap<Employee, Integer>(new EmployeeSorterAsPerSalary());
		
		map.put(new Employee(1, "raj", 90000), 45);
		map.put(new Employee(12, "ts", 91000), 25);
		map.put(new Employee(189, "sushant", 70000), 25);
		
		//System.out.println(map.get(new Employee(1, "raj", 90000)));
		
		Set<Entry<Employee, Integer>> entrySet=map.entrySet();
		
		for(Entry<Employee, Integer> entry: entrySet) {
			System.out.println(entry.getKey());
			System.out.println(entry.getValue());
			System.out.println("-----------");
		}
		
		
		
		
		
		//key as custom object ie Employee
		//GPP: it is a bad programming practice to take mutable class as key (bugs)
		//"u should always take key as immutable"
		
//		Map<String, Integer> map=new TreeMap<String, Integer>();
//		map.put("raj", 79);
//		map.put("ekta", 99);
//		map.put("syed", 90);
//		map.put("poojitha", 89);
//		map.put("neeraj", 91);
//		
//		//keyset vs entrySet
////		Set<String> keys=map.keySet();
////		for(String key: keys) {
////			System.out.println(key + ":"+ map.get(key));
////		}
////		
//		Set<Entry<String, Integer>> entrySet=map.entrySet();
//		
//		for(Entry<String, Integer> entry: entrySet) {
//			System.out.println(entry.getKey()+": "+ entry.getValue());
//		}
	}
}
