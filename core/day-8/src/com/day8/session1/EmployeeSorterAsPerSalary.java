package com.day8.session1;

import java.util.Comparator;

public class EmployeeSorterAsPerSalary implements Comparator<Employee> {

	@Override
	public int compare(Employee o1, Employee o2) {
		return Integer.compare(o2.getSalary(), o1.getSalary());
	}

}
