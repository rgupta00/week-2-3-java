package com.day8.session1.comparable_comparator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateExample {
	
	public static void main(String[] args) throws ParseException {
		String dateString="11/11/2011";
		SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		Date date=dateFormat.parse(dateString);
		
		System.out.println(date);
		
	}

}
