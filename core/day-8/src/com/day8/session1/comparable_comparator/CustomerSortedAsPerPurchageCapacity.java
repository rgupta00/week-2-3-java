package com.day8.session1.comparable_comparator;

import java.util.Comparator;

public class CustomerSortedAsPerPurchageCapacity implements Comparator<Customer>{

	@Override
	public int compare(Customer o1, Customer o2) {
		return Double.compare(o2.getPuchargeCap(),o1.getPuchargeCap() );
	}

}
