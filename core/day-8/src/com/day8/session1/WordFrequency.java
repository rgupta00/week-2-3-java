package com.day8.session1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class WordFrequency {

	public static void main(String[] args) {
		Map<String, Integer> map=new TreeMap<String, Integer>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("strory.txt"));
			String line = null;
			while ((line = br.readLine()) != null) {
				String words[]=line.split(":");
				
				for(String word: words) {
					word= word.toLowerCase();
					if(map.containsKey(word)) {
						Integer freq= map.get(word);
						map.put(word, ++freq);
					}else {
						map.put(word, 1);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//print the contents of map
		Set<Entry<String, Integer>> entrySet=map.entrySet();
		
		for(Entry<String, Integer> entry: entrySet) {
			System.out.println(entry.getKey()+" : "+ entry.getValue());
		}
	}

}
