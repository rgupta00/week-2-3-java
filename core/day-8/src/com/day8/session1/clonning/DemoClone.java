package com.day8.session1.clonning;

import java.util.Date;

import com.day8.session1.Employee;

public class DemoClone {
	
	public static void main(String[] args) throws CloneNotSupportedException {
		//clone method of object class is used to do clonning of the object
		//making a duplicate copy of the object
		//be on the safer side
		
		Customer customer=new Customer(1, "raj", 67, new Date());
		Customer customerCloned=(Customer) customer.clone();
		
		
		myFunction(customerCloned);
		System.out.println(customer);
		System.out.println(customerCloned);
		
		
	}

	private static void myFunction(final Customer customer) {
		//customer.setName("amit");
		customer.getDob().setDate(20);
	}

}
