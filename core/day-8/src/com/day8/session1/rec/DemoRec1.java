package com.day8.session1.rec;

public class DemoRec1 {

	public static int sum(int n) {
		if(n==1) {
			return 1;
		}else
			return n + sum(n-1);
	}
	public static void main(String[] args) {
		//sum of natual no  using rec
		
		System.out.println(sum(5));
	}
}
