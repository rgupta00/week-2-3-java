package com.empapp.web.controller;

import java.util.*;

import com.empapp.model.dao.Employee;
import com.empapp.model.dao.EmployeeNotFoundException;
import com.empapp.model.service.EmployeeService;
import com.empapp.model.service.EmployeeServiceImpl;

public class EmployeeAppTester {

	public static void main(String[] args) {
		EmployeeService employeeService = new EmployeeServiceImpl();

		List<Employee> employees = employeeService.getAllEmployee();

		System.out.println("------Printing all emp------");
		// printEmployees(employees);

		//System.out.println("----adding an emp------------");

		//Employee employee = new Employee(1235, "teja", 67, 25);

		// employeeService.addEmployee(employee);

		//System.out.println("----updating an emp------------");

		//employeeService.updateEmployee(1235, 72);

		
		//System.out.println("----delete an emp------------");

		//employeeService.deleteEmployee(2);
		
		System.out.println("find and print an employee");
		try{
			Employee employee=employeeService.getEmployeeByName("umesh");
			System.out.println(employee);
		}catch(EmployeeNotFoundException ex) {
			System.out.println(ex.getMessage());
		}
		
		//employees = employeeService.getAllEmployee();
		//printEmployees(employees);

	}

	private static void printEmployees(List<Employee> employees) {
		for (Employee employee : employees) {
			System.out.println(employee);
		}
	}
}
