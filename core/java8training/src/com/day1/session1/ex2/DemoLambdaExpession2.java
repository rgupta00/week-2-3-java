package com.day1.session1.ex2;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.*;
public class DemoLambdaExpession2 {

	
	public static void main(String[] args) {
		
		List<Book> books=Arrays.asList
				(new Book(121, "java", "raj", 240),
						new Book(11, "python", "ekta", 840),
						new Book(621, "c programming", "gunika", 300));
		
		//count all costly books
		long count=books.stream().filter(b-> b.getPrice()>=250).count();
		System.out.println(count);
		
		Function<Book, BookData> mapper= book-> new BookData(book.getTitle(), book.getPrice());
			
		
		
		
		//i want to print only title and price 
		List<BookData> bookDataList=books.stream()
				.map(book-> new BookData(book.getTitle(), book.getPrice()))
				.collect(toList());
		
		
		//print the name of all costly book >250
		List<String> names=books.stream()
				.sorted(Comparator.comparing(Book::getPrice).reversed())
				.filter(book-> book.getPrice()>=250)
				.map(book-> book.getTitle())
				.collect(Collectors.toList());
		names.forEach(name-> System.out.println(name));
	}
}
