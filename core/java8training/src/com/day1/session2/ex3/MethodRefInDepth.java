package com.day1.session2.ex3;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
class EmpData{
	private String name;
	private int salary;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "EmpData [name=" + name + ", salary=" + salary + "]";
	}
	public EmpData(String name, int salary) {
		super();
		this.name = name;
		this.salary = salary;
	}
	
	
}
class Emp{
	private int id;
	private String name;
	private String dept;
	private int salary;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
					//this
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public Emp(int id, String name, String dept, int salary) {
		this.id = id;
		this.name = name;
		this.dept = dept;
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Emp [id=" + id + ", name=" + name + ", dept=" + dept + ", salary=" + salary + "]";
	}
	//if a method is an instance method then java will pass "this" , this refer to current object'
	
	public boolean highSalaryGetter() {
		return salary>=150;
	}
	
	public static EmpData mapToEmpData(Emp emp) {
		return new EmpData(emp.getName(), emp.getSalary());
	}
	
}
public class MethodRefInDepth {

	public static void main(String[] args) {
		
		Function<Emp, Integer> mapper=emp-> emp.getSalary();
		
		Function<Emp, Integer> mapper2=Emp::getSalary;
		
		Function<Emp, EmpData> mapper3=emp-> new EmpData(emp.getName(), emp.getSalary());
		
		Function<Emp, EmpData> mapper4=Emp::mapToEmpData;
		
		List<String> data=Arrays.asList("java","python","compiler design");
		//data.forEach(s-> System.out.println(s));
		
		data.forEach(System.out::println);
		
		//150 pm high salary getter
		//Predicate T=> T/F
//		Predicate<Emp> highSalaryGetter=emp-> emp.getSalary()>= 150;
//		
//		Predicate<Emp> highSalaryGetter2=Emp::highSalaryGetter;
//		
//		Emp emp=new Emp(121, "raj", "it", 670);
//		
//		System.out.println(highSalaryGetter.test(emp));
	}
}




