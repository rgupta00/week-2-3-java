package com.day1.session2.ex3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.swing.text.html.parser.Entity;

import com.day1.session1.ex2.Book;

public class AppleTester {

		
		public static void main(String[] args) {

		List<Apple> apples = Arrays.asList(new Apple("red", 400), new Apple(
				"green", 300), new Apple("green", 200), new Apple("red", 250));
		
		//Most imp functional interface in java 8
		
		//Predicate	
			
		//Function
		
		//Consumer
		
		//biConsumer
		Map<String, Integer>map=new HashMap<String, Integer>();
		map.put("raj", 90);
		map.put("ekta", 92);
		map.put("suman", 60);
		map.put("amit", 92);
		map.put("kapil", 70);
		
		map.forEach(( name,  marks)-> System.out.println(name + ": "+ marks));
		//Supplier
	
		
		//BiFunction
	
		
		
		
	}
}

















