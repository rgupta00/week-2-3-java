package com.day1.session2.ex7;

import java.util.stream.LongStream;

class PrimeNumbers {
	
	public static boolean isPrime(long val) {
		for (long i = 2; i <= val / 2; i++) {
			if ((val % i) == 0) {
				return false;
			}
		}
		return true;
	}
}
public class ParellelStream {
	
	public static void main(String[] args) {
		
		//1 -10000
		//java 7 ways
//		
//		long counter=0;
//		long start=System.currentTimeMillis();
//		
//		for(long i=1; i<100000; i++) {
//			if(PrimeNumbers.isPrime(i)) {
//				counter++;
//			}
//		}
//		long end=System.currentTimeMillis();
//		
//		System.out.println("time taken: "+ (end-start));
//		System.out.println(counter);
		
		
		

		long counter=0;
		long start=System.currentTimeMillis();
		
		counter=LongStream.rangeClosed(1, 100000)
				.filter(PrimeNumbers::isPrime)
				.parallel()
				.count();
		long end=System.currentTimeMillis();
		
		System.out.println("time taken: "+ (end-start));
		System.out.println(counter);
		
		
		//declartive data procssing for || processing
		//race condtion
		
		
	}

}
