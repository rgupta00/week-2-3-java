package com.day1.session2.ex6;

import java.util.Optional;

public class DiffOptinalOfVsNullable {
	
	public static void main(String[] args) {
		//Optional<String> opString=Optional.empty();//thre is nothing in the box
		
		//Optional<String> opString2=Optional.of(null);
		//thre is nothing in the box if nothing throws the error immeditly
		
		Optional<String> opString3=Optional.ofNullable(null);//thre is nothing in the box
		//String val=opString3.get(); //BAD CODE
		
		String val=opString3.orElse("not found");
		
	}

}
