package com.day1.session2.ex6;

import java.util.Optional;

import org.graalvm.compiler.core.common.type.ArithmeticOpTable.Op;

/*
 * 		may have a	 may hv ins		but must have name
 * Person -----> Car---> Insurance--> name
 */

class Insurance{
	private String InsuranceName;

	public String getInsuranceName() {
		return InsuranceName;
	}

	public void setInsuranceName(String insuranceName) {
		InsuranceName = insuranceName;
	}
	
	
}

// Insurance is optional to the car

class Car{
	private Optional<Insurance> insurance;

	public Optional<Insurance> getInsurance() {
		return insurance;
	}

	public void setInsurance(Optional<Insurance> insurance) {
		this.insurance = insurance;
	}

	
	
}

//Person may have optinal car
class Person{
	private Optional<Car> car;

	public Optional<Car> getCar() {
		return car;
	}

	public void setCar(Optional<Car> car) {
		this.car = car;
	}


}

public class DemoOptional {
	public static void main(String[] args) {
		
		Insurance insurance=new Insurance();
		insurance.setInsuranceName("sun life");
	
	
		Optional<Insurance> inOpt=Optional.ofNullable(null);
		Car car =new Car();
		car.setInsurance(inOpt);
		Optional<Car> opCar=Optional.ofNullable(car);
		
		Person person=new Person();
		person.setCar(opCar);
		
		Optional<Person>opPerson=Optional.ofNullable(person);
		
		
		String name =processInsuranceCompanyDetails(opPerson);
		System.out.println(name);
	}

	private static String processInsuranceCompanyDetails(Optional<Person> opPerson) {
		//Optional<Optional<Car>> map = opPerson.map(p-> p.getCar());
		return opPerson.flatMap(p-> p.getCar()).flatMap(c-> c.getInsurance())
		.map(ins-> ins.getInsuranceName()).orElse("ins name is not found");
	}

	//private static void processInsuranceCompanyDetails(Person person) {
//		if(person!=null) {
//			Car car=person.getCar();
//			if(car!=null) {
//				Insurance insurance=car.getInsurance();
//				if(insurance!=null) {
//					System.out.println("insurance is not done");
//				}
//			}else
//				System.out.println("car is not there");
//		}else
//			System.out.println("no person is passed");
//		String insuranceCompanyName=person.getCar().getInsurance().getInsuranceName();
//		System.out.println(insuranceCompanyName);
	//}
}






