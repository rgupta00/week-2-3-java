package com.bankapp;

public class Account {
	private int id;
	private String name;
	private double balance;
	
	public Account(int id, String name, double balance) throws AccountCreationException{
		if(balance<1000) {
			throw new AccountCreationException("account can not be create with initial amount "+balance);
		}
		this.id = id;
		this.name = name;
		this.balance = balance;
	}
	public void deposit(double amount)throws OverFundException {
		double temp=balance+ amount;
		if(temp> 50_00_000) {
			throw new OverFundException("you cant not deposit more then  5L in saving acc please contact branch manager");
		}
		balance=temp;
	}
	public void withdraw(double amount) throws NotSufficientFundException {
		double temp=balance-amount;
		if(temp<1000) {
			throw new NotSufficientFundException("your account dont have sufficinent fund");
		}
		balance=temp;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Account [id=").append(id).append(", name=").append(name).append(", balance=").append(balance)
				.append("]");
		return builder.toString();
	}
	
	

}
