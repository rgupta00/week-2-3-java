package com.bankapp;

public class NotSufficientFundException  extends Exception{
	public NotSufficientFundException(String message) {
		super(message);
	}
}
