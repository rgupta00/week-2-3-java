package com.demo;
import java.util.*;
public class DemoStack {
	
	public static void main(String[] args) {
		
		String data="i love java java love me";
		String tokens[]=data.split(" ");
		Stack<String> stack=new Stack<String>();
		for(String token: tokens) {
			stack.push(token);
		}
	
		while(!stack.isEmpty()) {
			System.out.println(stack.pop());
		}
		
		/*Stack<String> stack=new Stack<String>();
		stack.push("raj");
		stack.push("ekta");
		stack.push("gunika");
		stack.push("keshav");
		
		while(!stack.isEmpty()) {
			System.out.println(stack.pop());
		}*/
	}

}
