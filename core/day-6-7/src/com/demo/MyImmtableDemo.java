package com.demo;
import java.util.*;
/// how to create our own immutable classes...
final class MyImmtable{
	final private int i;
	final private int []arr;
	final private String name;
	final private Date date;
	final private List<String> list;
	
	public MyImmtable(int i, int[] arr, String name, Date date, List<String> list) {
		this.i = i;
		//this.arr =arr;
		this.arr=Arrays.copyOf(arr, arr.length);
		this.name = name;
		this.date = date;
		this.list = list;
	}
	//never give setter 
	//can we give getter : (getter can also side effect"

	public int getI() {
		return i;
	}

	public int[] getArr() {
		return Arrays.copyOf(arr, arr.length);
	}

	public String getName() {
		return name;
	}

	public Date getDate() {
		return date;
	}

	public List<String> getList() {
		return list;
	}
	
	public void print() {
		System.out.println("i: "+ i);
		System.out.println("name: "+ name);
		System.out.println("date: "+ date);
		
		System.out.println("----arr data-------");
		for(int temp: arr) {
			System.out.print(temp+" ");
		}
		System.out.println();
		System.out.println("-----array list --------");
		
		for(String data: list) {
			System.out.print(data+" ");
		}
		
	}
	
}
public class MyImmtableDemo {

	
	public static void main(String[] args) {
		int i=11;
		int []arr= {4,6,7};
		Date date=new Date();
		List<String> list=new ArrayList<String>();
		list.add("amit");
		String name="raj";
		
		
		//int i, int[] arr, String name, Date date, List<String> list
		MyImmtable immtable=new MyImmtable(i, arr, name, date, list);
		
		immtable.print();
		
		arr[2]=555;
		
		immtable.print();
		
		int temp[]=immtable.getArr();
		temp[0]=2222;
		immtable.print();
	}
}
