package com.demo.exceptions;

import java.io.FileNotFoundException;
import java.io.IOException;

class A{
	public void foo()throws IOException {
		System.out.println("foo of A");
	}
}


class B extends A{
	public void foo() throws IOException, NullPointerException{
		System.out.println("foo of B");
	}
}
public class ExHandingWithCtr {
	
	public static void main(String[] args) {
		
	}

}
