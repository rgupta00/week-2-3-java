package com.demo.exceptions;

import java.io.FileNotFoundException;
import java.io.IOException;

class A1{
	A1() throws IOException{
		System.out.println("ctr of A1");
	}
}

//ctr of drived class can not throw smaller ex ( smaller in hierarchy)

class B1 extends A1{
	
	B1() throws Exception, ArithmeticException{
	
		System.out.println("ctr of B1");
	}
}
public class ExHandingWithOverriding {
	
	public static void main(String[] args) {
		
	}

}
