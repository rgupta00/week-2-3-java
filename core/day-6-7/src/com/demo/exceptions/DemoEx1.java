package com.demo.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DemoEx1 {

	public static void main(String[] args) {
		//to divide 2 nos
		Scanner scanner=null;
		try {
			 scanner=new Scanner(System.in);
			System.out.println("PE num");
			int num=scanner.nextInt();
			
			System.out.println("PE deno");
			int deno=scanner.nextInt();
			
			int result=num/deno;
			
			System.out.println(result);
		}
		catch(InputMismatchException ex) {
			System.out.println("please enter int only");
		
		}catch(ArithmeticException ex) {
			System.out.println("dont divide by zero");
		}finally {
			if(scanner!=null) {
				scanner.close();
			}
		}
		System.out.println("----i am running -----");
	}
}
