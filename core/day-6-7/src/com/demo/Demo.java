package com.demo;

import java.util.Stack;

class Student{
	private int id;
	private String name;
	private int marks;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	public Student(int id, String name, int marks) {
		this.id = id;
		this.name = name;
		this.marks = marks;
	}
	public Student() {}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Student [id=").append(id).append(", name=").append(name).append(", marks=").append(marks)
				.append("]");
		return builder.toString();
	}

	
	
	
	
	
	
//	@Override
//	public String toString() {
//		//return " id=" + id + ", name=" + name + ", marks=" + marks ;
////		return new StringBuilder()
////				.append("id=")
////				.append(id)
////				.append("name=")
////				.append(name)
////				.append("marks=")
////				.append(marks)
////				.toString();
//	}
	
	
	
}
public class Demo {
	public static void main(String[] args) {
		
		//String tokens (imp)
		
		
	
		/*
		 * 1. length()
		2. charAt()
		3. indexOf()
		4. SubString()
		5. equal/equalsIgnoreCase() - String Comparison 
		6. trim()
		7. replace()
				String date= "11/11/2011";
				String date2=date.replace("/", "-");
				System.out.println(date2);
	
		 */
		
		//length
		//String data="i love java";
		//System.out.println(data.length());
		
		//charAt
		//String data="i love java";
		//System.out.println(data.charAt(data.length()-2));
		
		// indexOf()
		//String data="i love i java";
		//System.out.println(data.indexOf('i',5));
		
		// SubString()
//		String data="i love iw java";
//		String part=data.substring(4,data.length());
//		System.out.println(part);
		//intern method in string class?
		
		//from the ui 
//		String data=" rajeev gupta       ";
//		String dataRemoveSpaces=data.trim();
//		System.out.println(dataRemoveSpaces);
		
		//replace()
//		String date= "11/11/2011";
//		String date2=date.replace("/", "-");
//		System.out.println(date2);
		
		
		/*
		 * String s1="java"; String s2=new String("java");
		 * 
		 * if(s1==s2) System.out.println("address  are same"); else
		 * System.out.println("Add is not same");
		 */
		
//		Student student=new Student(121, "rajat", 70);
//		System.out.println(student);
		
		//what is the best way to cat a string
//		String myData="i"+"love"+"teaching"+"i"+"feel"+"involve"+"with"+"it";
//		String myDataRightWay= new StringBuilder()
//				.append("i")
//				.append("love")
//				.append("teaching")
//				.append("i")
//				.toString();
		
		//how to reverse the string
		
//		String s="india";
//		
//		StringBuilder builder=new StringBuilder(s);
//		String rev=builder.reverse().toString();
//		System.out.println(rev);
		
		
//		//how to revrese a string in java
//		
//		MyStack myStack=new MyStack();
//		String data="india";
//		char chars[]=data.toCharArray();
//		
//		for(int i=0; i< chars.length; i++) {
//			myStack.push(chars[i]);
//		}
//		
//		while(!myStack.isEmpty()) {
//			System.out.print(myStack.pop());
//		}
//		
		
		//how to reverse a string object
//		
//		Stack<Character>charStack=new Stack<Character>();
//		
//	
//		String name="india";
//		
//		char []chars= name.toCharArray();
//		for(char temp : chars) {
//			charStack.push(temp);
//		}
//		
//		for(char temp: charStack) {
//			System.out.println(charStack.pop());
//		}
//		
		
		/*
		 * 1. length()
		2. charAt()
		3. indexOf()
		4. SubString()
		5. equal/equalsIgnoreCase() - String Comparison 
		6. trim()
		7. replace()
		String date= "11/11/2011";
			String date2=date.replace("/", "-");
			System.out.println(date2);
		 */
		
		
		
//		String name="rajeev gupta";
//
//		String name2= name.toUpperCase();
//		
//		System.out.println(name);
//		System.out.println(name2);
		
//		String name="rajeev gupta";
//
//		name.toUpperCase();
//		System.out.println(name);
		
//		String data=2+2+" raj "+ 2+2;
//		System.out.println(data);
		
		//What is misuse of string class
		String a="in"+"pak"+"chinna"+"us";
		//System.out.println(a);
		
//		String s1=new String ("in");
//		String s2="in";
//		
//		if(s1==s2)
//			System.out.println("are eq");
//		else
//			System.out.println("not eq");
		
		///Student student=new Student(12, "raj", 80);
		
//		String data="india is my country";//A
//		String data2=new String("india is my country");//B
		
		
	}
}






