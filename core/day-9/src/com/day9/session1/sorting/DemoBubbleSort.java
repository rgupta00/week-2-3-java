package com.day9.session1.sorting;

public class DemoBubbleSort {

	public static void main(String[] args) {
		int a[]= {38,52,9,18, 6,62,13};
		int temp=0;
		
		boolean swap=true;
		
		for(int i=0;i<a.length&&swap; i++) {
			swap=false;
			
			for(int j=0;j<a.length-1-i; j++) {
				if(a[j]>a[j+1]) {
					temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
					
					swap=true;
				}
			}
		}
		
		printArr(a);
	}

	private static void printArr(int[] a) {
		System.out.println("printing sorted array:");
		for(int i=0;i<a.length; i++) {
			System.out.print(a[i]+ " ");
		}
		System.out.println();
	}
}
