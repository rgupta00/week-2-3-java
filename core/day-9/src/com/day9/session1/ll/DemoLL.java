package com.day9.session1.ll;

class LinkList{
	
	class Node{
		int info;
		Node next;
		public Node(int info) {
			this.info = info;
			this.next=null;
		}
	}
	Node first=null, last=null;
	
	public void insertFirst(int data) {
		Node newNode=new Node(data);
		if(first==null) {
			first=last=newNode;
		}else {
			newNode.next=first;
			first=newNode;
		}
	}
	public void insertLast(int data) {
		Node newNode=new Node(data);
		if(first==null) {
			first=last=newNode;
		}else {
			last.next=newNode;
			last=newNode;
		}
	}
	
	public void insert(int data) {
		Node newNode=new Node(data);
		if(first==null) {
			first=last=newNode;
		}else {
			last.next=newNode;
			last=newNode;
		}
	}

	public void print() {
		if(first==null) {
			System.out.println("no node to show");
		}else {
			Node temp=first;
			while(temp!=null) {
				System.out.print(temp.info+" ");
				temp=temp.next;
			}
		}
	}
}
public class DemoLL {

	public static void main(String[] args) {
		LinkList ll=new LinkList();
		ll.insert(33);
		ll.insert(3);
		ll.insert(93);
		ll.insert(8);
		ll.insert(5);
		ll.insertLast(555);
		ll.print();
	}
}
