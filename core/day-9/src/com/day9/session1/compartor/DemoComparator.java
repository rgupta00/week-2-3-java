package com.day9.session1.compartor;
import java.util.*;
public class DemoComparator {

	public static void main(String[] args) {
		List<Student> list=new ArrayList<Student>();
		list.add(new Student(12, "raj", 49));
		list.add(new Student(1, "amit", 50));
		list.add(new Student(120, "sumit", 47));		
		list.add(new Student(172, "ekta", 42));
		
		
		Collections.sort(list);
		
		print(list);
		
		Comparator<Student>comparatorForMarks=new Comparator<Student>() {
			@Override
			public int compare(Student o1, Student o2) {
				return Integer.compare(o2.getMarks(), o1.getMarks());
			}
		};
		
		Collections.sort(list, comparatorForMarks);
		System.out.println("-----printing sorted as per marks---");
		print(list);
		
		
		Collections.sort(list, new Comparator<Student>() {

			@Override
			public int compare(Student o1, Student o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		System.out.println("-----printing sorted as per names---");
		print(list);
		
	}

	private static void print(List<Student> list) {
		for(Student student: list) {
			System.out.println(student);
		}
	}
}
