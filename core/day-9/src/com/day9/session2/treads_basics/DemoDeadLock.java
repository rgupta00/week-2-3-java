package com.day9.session2.treads_basics;

public class DemoDeadLock {
	// jps -l
	//jstack pid > demo.txt
	
	public static void main(String[] args) {
		final String r1="resouce1";
		final String r2="resouce2";
		
		Thread thread1=new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized (r1) {
					System.out.println("thread1 got lock on r1");
					try {
						Thread.sleep(100);
					}catch(InterruptedException ex) {}
					synchronized (r2) {
						System.out.println("thread1 got lock on r2");
					}
				}
			}
		});
		
		Thread thread2=new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized (r1) {
					System.out.println("thread2 got lock on r1");
					try {
						Thread.sleep(100);
					}catch(InterruptedException ex) {}
					synchronized (r2) {
						System.out.println("thread2 got lock on r2");
					}
				}
			}
		});
		
		thread1.start();
		thread2.start();
	}

}
