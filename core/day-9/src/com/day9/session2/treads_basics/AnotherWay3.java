package com.day9.session2.treads_basics;

public class AnotherWay3 {

	public static void main(String[] args) {

     Thread thread = new Thread(() ->System.out.println("thread is running"));
	thread.start();
		
//		new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				System.out.println("thread is running");
//			}
//		}).start();

//		Thread thread = new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
//				System.out.println("thread is running");
//			}
//		});
//		thread.start();

//		Runnable runnable=new Runnable() {
//			
//			@Override
//			public void run() {
//				System.out.println("job of thread....");
//			}
//		};
//		
//		Thread thread=new Thread(runnable);
//		thread.start();
	}
}
