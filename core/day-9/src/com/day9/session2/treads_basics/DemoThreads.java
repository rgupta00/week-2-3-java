package com.day9.session2.treads_basics;
/*
 * 2 ways:
 * Thread
 * Runnable: better 
 * 
 * 	Think impl of runnable as a job
 * 	think thread like a worker
 */

class Job implements Runnable{

	@Override
	public void run() {
		System.out.println("job that need to be done by threads:"+Thread.currentThread().getName());
	}
	
}
public class DemoThreads {

	public static void main(String[] args) {
		System.out.println("i ma inside main");
		System.out.println(Thread.currentThread().getName()+" : "+ Thread.currentThread().getPriority());
		
		Job job=new Job();// job
		
		Thread thread=new Thread(job, "A");
		
		//thread.run();//no threads is spawn
		
		thread.start();// it will finally call run() //when it call run : when cpu is availble
		//Thread Scheduler : not part of jvm : OS
		
		//threds are PD
		
		System.out.println(" main is finished");
		
	}
}








