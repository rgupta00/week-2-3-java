package com.day9.session2.treads_basics;
class MyJob2 implements Runnable{
	private Thread thread;

	public MyJob2(String name) {
		thread=new Thread(this, name);
		thread.start();
	}
	@Override
	public void run() {
		System.out.println("thread is running : "+ Thread.currentThread().getName());
	}
	
}
public class AnotherWay2 {

	public static void main(String[] args) {
		MyJob2 job2=new MyJob2("foo");
		
	}
}
