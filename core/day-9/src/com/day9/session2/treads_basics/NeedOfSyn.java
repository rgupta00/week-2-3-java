package com.day9.session2.treads_basics;
class Demo{
	
}
class Printer {
	// i want to foce a thread to take a lock to call this method ...
	private Object lock=new Object();
	public  void print(String letter) {
		
		//
		///
		synchronized (lock) {
			System.out.print("[" + letter);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			System.out.println("]");
			
		}
		
		//
		///
		
	}
}

class Client implements Runnable {
	private Printer printer;
	private String letter;
	private Thread thread;

	public Client(Printer printer, String letter) {
		this.printer = printer;
		this.letter = letter;
		this.thread = new Thread(this);
		thread.start();
	}

	@Override
	public void run() {
		printer.print(letter);
	}

}

public class NeedOfSyn {

	public static void main(String[] args) {
		Printer printer=new Printer();
		Client client1=new Client(printer, "i love java");
		Client client2=new Client(printer, "i love python");
		
		Client client3=new Client(printer, "MTech project report");
		
	}
}




