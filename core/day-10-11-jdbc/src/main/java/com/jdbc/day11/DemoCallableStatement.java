package com.jdbc.day11;

import java.sql.*;

import com.jdbc.day10.ConnectionFactory;

public class DemoCallableStatement {

	public static void main(String[] args) {
		Connection connection=ConnectionFactory.getConnection();
		try {
			CallableStatement callableStatement=connection.prepareCall("call add12(?,?,?)");
			callableStatement.setInt(1, 4);
			callableStatement.setInt(2, 4);
			callableStatement.registerOutParameter(3, Types.INTEGER);
			callableStatement.execute();
			System.out.println(callableStatement.getInt(3));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
