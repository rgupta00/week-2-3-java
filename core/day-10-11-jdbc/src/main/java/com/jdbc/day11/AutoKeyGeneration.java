package com.jdbc.day11;

import java.sql.*;

import com.jdbc.day10.ConnectionFactory;

public class AutoKeyGeneration {

	public static void main(String[] args) throws SQLException {
		Connection connection=ConnectionFactory.getConnection();
		String query="insert into customer(customerName, customerAddress) values (?,?)";
		PreparedStatement pstmt=connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		pstmt.setString(1, "amit");
		pstmt.setString(2, "btm");
		int no=pstmt.executeUpdate();
		// if no is >0 then query was successful ie we can ask him to give auto gen key
		if(no>0) {
			   ResultSet rs = pstmt.getGeneratedKeys();
			    rs.next();
			   int autogenId = rs.getInt(1);
			   System.out.println(autogenId);
		}
		
	}

}
