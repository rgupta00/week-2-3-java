package com.jdbc.day11;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.jdbc.day10.ConnectionFactory;

public class JdbcFetchSize {

	public static void main(String[] args) throws SQLException {
		
		Connection connection = ConnectionFactory.getConnection();
		long start=System.currentTimeMillis();
		
		PreparedStatement pstmt=connection.prepareStatement("select * from mytemp");
		pstmt.setFetchSize(1000);
		// 10K/10--> 1000
		// 10K/100--> 100
		
		ResultSet rs=pstmt.executeQuery();
		while(rs.next()) {
			System.out.println(rs.getString(1));
		}
		long end=System.currentTimeMillis();
		
		System.out.println("time taken: "+ (end-start));
	}
}
