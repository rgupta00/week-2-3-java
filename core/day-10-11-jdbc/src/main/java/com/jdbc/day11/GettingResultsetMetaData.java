package com.jdbc.day11;

import java.sql.*;

import com.jdbc.day10.ConnectionFactory;


public class GettingResultsetMetaData {
	
	public static void main(String[] args) throws SQLException {
		//ResultSetMetaData
		//dataBasMetaData
		
		Connection connection=ConnectionFactory.getConnection();
		
		DatabaseMetaData dbmd=connection.getMetaData();  
		  
		System.out.println("Driver Name: "+dbmd.getDriverName());  
		System.out.println("Driver Version: "+dbmd.getDriverVersion());  
		System.out.println("UserName: "+dbmd.getUserName());  
		System.out.println("Database Product Name: "+dbmd.getDatabaseProductName());  
		System.out.println
		("Database Product Version: "+dbmd.getDatabaseProductVersion());

		
		
		
		PreparedStatement pstmt=connection.prepareStatement("select * from emp2");
		ResultSet rs=pstmt.executeQuery();
		ResultSetMetaData rsMeta=rs.getMetaData();
		
		System.out.println(rsMeta.getColumnCount());
		
		for(int i=1;i<=rsMeta.getColumnCount();i++){
			System.out.println(rsMeta.getColumnClassName(i)+":"+rsMeta.getColumnLabel(i));
		}
	}

}
