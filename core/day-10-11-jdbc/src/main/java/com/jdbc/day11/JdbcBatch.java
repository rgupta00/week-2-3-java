package com.jdbc.day11;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.jdbc.day10.ConnectionFactory;

public class JdbcBatch {
	
	
	public static void main(String[] args) throws SQLException {
		Connection connection = ConnectionFactory.getConnection();

		PreparedStatement pstmt = connection
				.prepareStatement("insert into mytemp(name) values (?)");
				
		connection.setAutoCommit(false);//*
				
		for (long i = 0; i < 10000; i++) {
			pstmt.setString(1, "foo:" + i);
			   pstmt.addBatch();
				if (i % 200 == 0) {
					pstmt.executeBatch();
				}
			}
		connection.commit();


		connection.rollback();
	}

}
