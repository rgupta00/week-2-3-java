package com.jdbc.day11;

import java.sql.*;

import com.jdbc.day10.ConnectionFactory;

public class TransationsACID {
	//create table account(id int not null, name varchar(50), int balance not null);
	public static void main(String[] args) {
		Connection connection = ConnectionFactory.getConnection();
		
		try {
			connection.setAutoCommit(false);
			PreparedStatement pstmt=connection
					.prepareStatement("update account set balance=balance-100 where id=?");
			pstmt.setInt(1, 45);
			int val=pstmt.executeUpdate();// db is going to hit immediatly :(
		
			pstmt=connection.prepareStatement("update account set balance=balance+100 where id=?");
			pstmt.setInt(1, 55);
			if(1==1)
			throw new RuntimeException();
			
			pstmt.executeUpdate();
			connection.commit();
		
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			
		}
	}
}
