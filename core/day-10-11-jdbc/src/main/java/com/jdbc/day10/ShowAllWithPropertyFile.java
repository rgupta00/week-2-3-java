package com.jdbc.day10;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class ShowAllWithPropertyFile {

	public static void main(String[] args) {

		try {
			Connection connection = ConnectionFactory.getConnection();
			
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("select * from emp2");// pointer to recs

			while (rs.next()) {
				System.out.println(rs.getInt("id") + ": " + rs.getString("name") + ": " + rs.getInt("salary") + " : "
						+ rs.getInt("age"));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
