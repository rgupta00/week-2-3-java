package com.jdbc.day10;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;
public class AddEmp {

	public static void main(String[] args) {


				try (Connection connection =ConnectionFactory.getConnection()) {
					PreparedStatement pstmt=connection.prepareStatement
							("insert into emp2(id,name, salary, age) values(?,?,?,?)");
					pstmt.setInt(1, 196);
					pstmt.setString(2, "tarun");
					pstmt.setInt(3, 53);
					pstmt.setInt(4, 44);
					
					int noOfRowsEffected= pstmt.executeUpdate();
					System.out.println(noOfRowsEffected);
					
					
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
	}
}
