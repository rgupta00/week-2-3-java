package com.jdbc.day10;

import java.sql.*;

public class ShowAll {

	public static void main(String[] args) {
		
		//Loose coupling and high cohesion
		
		// 1. load the driver, dynamic class loading?
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("driver is loaded");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		// ARM : automatic resorce mgt
		try (Connection connection = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/cgi_demo?useSSL=false&allowPublicKeyRetrieval=true",
				"root", "root")) {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("select * from emp2");// pointer to recs

			// executeQuery vs executeUpdate
			// executeQuery: it dont change the state of db
			// it give rs

			// executeUpdate: it change the state of data
			// del, save, update: executeUpdate
			// executeUpdate give u no of rows effective

			while (rs.next()) {
				System.out.println(rs.getInt("id") + ": " + rs.getString("name") + ": " + rs.getInt("salary") + " : "
						+ rs.getInt("age"));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}
