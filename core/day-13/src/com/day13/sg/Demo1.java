package com.day13.sg;

import java.util.Random;
import java.util.function.IntPredicate;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Demo1 {
	
	public static void main(String[] args) {
		
		
		
		Stream.of(1, 2, 3, 4, 5)
				//.peek(i-> System.out.println("before map: "+ i))
				.map(i -> i * i)
				//.peek(i-> System.out.println("after map: "+ i))
				.forEach(System.out::println);
		
		//Primitive Versions of Predicate Interface
		
		//IntPredicate intPredicate= x->x%2==0;
		
		//IntStream.range(1, 100).filter(predicate)
		
		
		//Constructor References
		//Supplier<String> newString = String::new;
		
//		Supplier<String> suppString=() -> new String("raj");
//		
//		Supplier<String> suppStringAlternative=String :: new;
//			
//		String val=suppString.get();
//		
//		System.out.println("value: "+ val);
		
		
		
		//Stream.generate
		
//		Random random = new Random();
//		Stream.generate(random::nextBoolean)
//		.limit(2)
//		.forEach(System.out::println);
		
		
//		Function<String, Integer> parseInt = Integer::parseInt;
//		Function<Integer, Integer> absInt = Math::abs;
//		Function<String, Integer> parseAndAbsInt = parseInt.andThen(absInt);
//		Arrays.stream("4, -9, 16".split(", "))
//		.map(parseAndAbsInt)
//		.forEach(System.out::println);
//		
//		Arrays.stream("4, -9, 16".split(", "))
//		.map(Integer::parseInt)
//		.map(i -> (i < 0) ? -i : i)
//		.forEach(System.out::println);
	}

}
