package com.day13.sg;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WordStatistics {

	public static void main(String[] args) {
		String data = "There was a young lady named Bright " +
				"who traveled much faster than light " +
				"She set out one day " +
				"in a relative way " +
				"and came back the previous night ";
		
		 IntSummaryStatistics summaryStatistics = 
				 Pattern.compile(" ")
		.splitAsStream(data)
		.map(w-> w.trim())
		.mapToInt(w-> w.length())
		.summaryStatistics();
		
		 System.out.println("Max length: "+summaryStatistics.getMax());
		 System.out.println("min length: "+summaryStatistics.getMin());
		 System.out.println("count"+summaryStatistics.getCount());
		 
		 
		 System.out.println(summaryStatistics.getMax());
		 
		 
		
	}
}





