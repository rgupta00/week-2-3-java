package com.day13.sg;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

public class CalculationMethods {

	public static void main(String[] args) {
		
		Comparator<String>keyExtractor=( o1,  o2)-> Integer.compare(o1.length(), o2.length());
		
		
		
		String data="i love java and teaching and traveling";
		String tokens[]=data.split(" ");
		Optional<String> min = Arrays
				.stream(tokens)
				.max(( o1,  o2)-> Integer.compare(o1.length(), o2.length()));
		System.out.println(min.orElse("not found"));
	}
}
