package com.day13.bookstore;
import static java.util.stream.Collectors.*;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.IntBinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;



public class CopyOfDemoBookCaseStudyProblem {

	public static void main(String[] args) {

		List<Book> allBooks = loadAllBooks();

		// 1. Find books with more then 400 pages
//		List<Book> booksMoreThen400Pages=allBooks
//				.stream()
//				.filter(book-> book.getPages()>=400)
//				.collect(Collectors.toList());
//		booksMoreThen400Pages.forEach(book-> System.out.println(book));
		
		// 2. Find all books that are java books and more then 400 pages
		
		Predicate<Book>moreThen400pages=book-> book.getPages()>400;
		Predicate<Book>javaBookPredicate=book-> book.getSubject()==Subject.JAVA;
		
		List<Book>booksJavaAndMoreThen400Pages=allBooks.stream()
				.filter(moreThen400pages.and(javaBookPredicate))
				.collect(Collectors.toList());
		//booksJavaAndMoreThen400Pages.forEach(book-> System.out.println(book));
		
		// 3. We need the top 2 longest books
//		List<Book> top2LongestBooks=allBooks
//				.stream()
//				.sorted(Comparator.comparing(Book::getPages).reversed())
//				.limit(2)
//				.collect(Collectors.toList());
//			
//		top2LongestBooks.forEach(b-> System.out.println(b))
		;
		// 4. We need from the fourth to the last longest books
//		List<Book> top2LongestBooks=allBooks
//				.stream()
//				.sorted(Comparator.comparing(Book::getPages).reversed())
//				.skip(1)
//				.limit(1)
//				.collect(Collectors.toList());
//			
//		top2LongestBooks.forEach(b-> System.out.println(b));
		// 5. We need to get all the publishing years

		List<Integer> allPubYear= allBooks
				.stream()
				.map(book-> book.getYear())
				.sorted()
				.distinct()
				.collect(toList());
		//allPubYear.forEach(year-> System.out.println(year));
		// 6. We need all the authors names who have written a book		
		
		//flatMap => map : convert something to something else
		
		//Stream<Stream<Author>> map = allBooks.stream().map(book-> book.getAuthors().stream());
		
		List<String> authorNames= allBooks
				.stream()
				.flatMap(book->book.getAuthors().stream())
				.map(author-> author.getName())
				.distinct()
				.collect(toList());
		
	//	authorNames.forEach(name-> System.out.println(name));
		
		// We need all the origin countries of the authors

		
		
		// We want the most recent published book.
		
//		Optional<Book> maxOp =
//				allBooks.stream().min(Comparator.comparing(Book::getYear));
//		
//		String title= maxOp.map(b-> b.getTitle()).orElse("not found");
//		System.out.println(title);
	
		// We want to know if all the books are written by more than one author
	
//		boolean allBooksWrittenByMoreOneAuthor = allBooks.stream().allMatch(b-> b.getAuthors().size() >1);
//		
//		System.out.println(allBooksWrittenByMoreOneAuthor);
//		
//		//nonemath
//		if(allBooks.stream().noneMatch(b-> b.getPages()>=600)) {
//			System.out.println("true...");
//		}else
//			System.out.println("false...");
//		
		
		// We want one of the books written by more than one author. (findAny)
		
		//long sumOfPages=allBooks.stream().map(b-> b.getPages()).reduce(identity, accumulator);
		//System.out.println(sumOfPages);
		//
		
		// We want the total number of pages published.

//		long totalPages=allBooks.stream()
//				.map(b-> b.getPages())
//				.reduce(0, (x,y)->x+y);
//		
//		System.out.println(totalPages);
		
//		long totalPages=allBooks.stream()
//				.map(b-> b.getPages())
//				.reduce(0, Integer::sum);
//		
//		
//		System.out.println(totalPages);
//		
		
		
		//total no of pages fo all books
	//	long totalPages=allBooks.stream().mapToInt(b-> b.getPages()).sum();
		//System.out.println(totalPages);
		
		//Integer value = allBooks.stream().map(book-> book.getPages()).reduce(0, (a,b)->a+b);
		
		//System.out.println(value);
		
		//OptionalInt optNoOfPages = allBooks.stream().mapToInt(book-> book.getPages()).reduce((x,y)-> x+y);
		
		//System.out.println(optNoOfPages.orElse(0));
		
		// We want to know how many pages the longest book has.

		//allBooks.stream().mapToInt(b-> b.getPages()).reduce(Integer::max);
		
		//OptionalInt optMax = allBooks.stream().mapToInt(b-> b.getPages()).reduce(( a,  b)->  a>b?a:b);
		
		//System.out.println(optMax.orElse(-1));
		
		
		//OptionalInt optMax = allBooks.stream().mapToInt(b-> b.getPages()).reduce(op)
		
		
		// We want the average number of pages of the books
		
		Double collect = allBooks.stream().collect(Collectors.averagingInt(Book::getPages));
	
		// We want all the titles of the books

		//String nameOfBooks = allBooks.stream().map(Book::getTitle).collect(Collectors.joining(","));
		//System.out.println(nameOfBooks);
		// We want the book with the higher number of authors?

	
		// We want a Map of book per year.

		Map<Integer, List<Book>> map = allBooks.stream().collect(Collectors.groupingBy(Book::getYear));
		
		map.forEach((year, list)->System.out.println(year +": "+ list));
		// We want to count how many books are published per year.

	

	}

	private static List<Book> loadAllBooks() {
		List<Book> books = new ArrayList<Book>();
		List<Author> authors1 = Arrays.asList(new Author("raj", "gupta", "in"),
				new Author("ekta", "gupta", "in"));

		List<Author> authors2 = Arrays.asList(new Author("raj", "gupta", "in"));

		List<Author> authors3 = Arrays.asList(new Author("gunika", "gupta", "us"),
				new Author("keshav", "gupta", "us"));
		
		List<Author> authors4 = Arrays.asList(new Author("anju", "kumari", "in"));
		
		//Book book=new Book(title, authors, pages, subject, year, isbn)
		books.add(new Book("java", authors1, 300, Subject.JAVA, 2000, "1213"));
		books.add(new Book("adv java", authors2, 379, Subject.JAVA, 2007, "1218"));
		books.add(new Book(".net runtime arch", authors3, 300, Subject.DOT_NET, 2000, "1293"));
	books.add(new Book("oracle SQL funda", authors4, 500, Subject.ORACLE, 2020, "1200"));
		

		return books;
	}

}
