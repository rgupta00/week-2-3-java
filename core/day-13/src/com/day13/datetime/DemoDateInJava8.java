package com.day13.datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DemoDateInJava8 {
	
	public static void main(String[] args) {
		
		LocalDate date=LocalDate.now();
		LocalDate date2=date.minusDays(1);
		System.out.println(date2);
		
//		LocalDateTime dateTime=LocalDateTime.now();
//		System.out.println(dateTime);
		
//		LocalTime localTime=LocalTime.now();
//		System.out.println(localTime);
		
		
		//LocalDate
		//LocalDate date=LocalDate.of(2021, Month.JANUARY	, 21);
		//System.out.println(date);
		
//		LocalDate date=LocalDate.now();
//		System.out.println(date);
//		DateTimeFormatter formatter=DateTimeFormatter.ofPattern("dd/MM/yyyy");
//		
//		LocalDate date=LocalDate.parse("11/11/2012", formatter);
//		
//		System.out.println(date);
		
		//LocalTime
		//LocalDateTime
		//Instance
		//joda data and time api
		
//		Date date=new Date(11,13,11);//mutable, thread safe
//		System.out.println(date);
	}

}
