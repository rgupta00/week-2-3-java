package com.day13.session1;

import java.util.*;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class CantProcessStreamsTwice {

	public static void main(String[] args) {
		List<Integer> list=Arrays.asList(4,6,7,8,9);
		//Lazy evluation of streams
		list.stream().filter( t-> {
				System.out.println(t);
				return t%2==0;
			}
		).count();
		
	}
}
