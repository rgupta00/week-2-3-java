package com.day13.session1;
import java.util.*;
public class InternalVsExtrenalIteration {
	
	public static void main(String[] args) {
		List<Integer> list=Arrays.asList(4,6,7,8,9);
		list.stream().parallel().forEach(e-> System.out.println(e));//internal iteration:
		//jvm decide how to iteration, u just told java to iteration
		
		//extrenal iteration : u decide how to iteration
		for(Integer l: list) {
			System.out.println(l);
		}
	}

}
